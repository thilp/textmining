package textmining

/**
 * Represents an index generation.
 *
 * @param wordsToDids Mapping between a word and its corresponding document IDs
 */
class Generation(val wordsToDids: Map[String, Set[Int]])


