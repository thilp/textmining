package textmining

import scala.collection.mutable.ArrayBuffer

class Posting(val word: String, val urls: ArrayBuffer[String] = new ArrayBuffer[String]())

object Posting {
  type HashMap[A,B] = collection.mutable.HashMap[A,B]

  def index(documents: Iterable[TokenizedDocument]): Iterable[Posting] = {
    val wordCache = new HashMap[String, Posting]
    for (doc <- documents)
      for (word <- doc.words)
        wordCache.getOrElseUpdate(word, new Posting(word)).urls.append(doc.url)
    wordCache.values
  }
}