package textmining

import java.io.{FileInputStream, ObjectInputStream, FileOutputStream, ObjectOutputStream}
import scala.collection.{immutable, mutable}

/**
 * Maps URLs and words to document IDs, so that the [[textmining.Searcher]]
 * can find URLs associated with a given word.
 *
 * @param urlToDid a map whose keys are URLs and values are the corresponding DID
 * @param wordToDids a map whose keys are words and values are the set of corresponding DIDs
 */
class Index(val urlToDid: immutable.HashMap[String, Int],
            val wordToDids: immutable.HashMap[String, immutable.HashSet[Int]]
             ) extends Serializable {

  /**
   * Saves this object in the filesystem.
   * @param path name of the file used as storage
   * @throws java.io.FileNotFoundException if <code>path</code> does not represent a valid location.
   * @throws java.io.IOException if an I/O error happened.
   */
  def save(path: String) {
    Index.save(this, path)
  }
}

/** Contains methods used to build, save and load Indexes */
object Index {
  var lastDid = 0

  /**
   * Builds a new [[textmining.Index]] from a [[textmining.Posting]] set.
   *
   * Document IDs (DIDs) are generated here, on-the-fly.
   *
   * @param postings a list of [[textmining.Posting]]
   * @return a new [[textmining.Index]]
   */
  def build(postings: Iterable[Posting]): Index = {
    val w2ds = new mutable.HashMap[String, immutable.HashSet[Int]]
    val u2d = new mutable.HashMap[String, Int]

    for (p <- postings) {
      w2ds.update(p.word,
        w2ds.getOrElseUpdate(p.word, new immutable.HashSet[Int]) ++
          p.urls.map(u2d.getOrElseUpdate(_, {
            lastDid += 1;
            lastDid
          }))
      )
    }

    new Index(immutable.HashMap(u2d.toList: _*),
      immutable.HashMap(w2ds.toList: _*)
    )
  }

  /**
   * Effectively saves a [[textmining.Index]] on the filesystem.
   *
   * This method is implemented here (instead of the companion class)
   * because the symmetric "load" operation is here too. There is also
   * a <code>save</code> method in the companion class.
   *
   * @param index the object to be serialized
   * @param path the serialization location on the filesystem
   * @throws java.io.FileNotFoundException if <code>path</code> does not
   *                                       represent a valid location.
   * @throws java.io.IOException if an I/O error happened.
   */
  def save(index: Index, path: String) {
    val out = new ObjectOutputStream(new FileOutputStream(path))
    out.writeObject(index)
    out.close()
  }

  /**
   * Creates a [[textmining.Index]] object from a file located on disk.
   *
   * @param path the file location
   * @return the loaded object
   * @throws java.io.FileNotFoundException if <code>path</code> does not represent a valid location.
   * @throws java.io.IOException if an I/O error happened.
   * @throws java.lang.ClassNotFoundException if the file is not a valid serialized index.
   */
  def load(path: String): Index = {
    val in = new ObjectInputStream(new FileInputStream(path))
    in.readObject().asInstanceOf[Index]
  }
}
