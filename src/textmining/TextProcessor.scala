package textmining

trait TextProcessor {
  def process(word: String): String
}