package textmining

import java.io.{FileNotFoundException, File}
import scala.io.Source

/**
 * Represents the contents and the address of a resource (e.g. a file).
 * @param url the resource's address
 * @param text the resource's text contents
 */
class Document(val url: String, val text: String)


/** Factory for [[textmining.Document]] instances. */
object Document {

  private def extractText(f: File): Option[String] = try {
    Some(Source.fromFile(f, "UTF-8").mkString)
  } catch {
    case _: Throwable => None
  }

  /**
   * Creates a new <code>Document</code> from a given [[java.io.File]].
   * <p>
   * This function returns <code>None</code> if it was not possible to get the contents
   * of <code>f</code> as a valid, UTF-8 encoded string.
   *
   * @param f the [[java.io.File]] represented by the result
   * @return a [[scala.Option]] on [[textmining.Document]]
   * @throws SecurityException if a required system property value cannot be accessed.
   */
  def apply(f: File): Option[Document] = {
    if (!f.isFile || !f.canRead || f.length < 2) None
    extractText(f) match {
      case Some(string) => Some(new Document(f.getAbsolutePath, string))
      case _ => None
    }
  }

  /**
   * Creates a new document from the given filename.
   *
   * @param s the filename
   * @return a [[scala.Option]] on [[textmining.Document]]
   * @throws NullPointerException if the <code>s</code> argument is null.
   * @throws SecurityException if a required system property value cannot be accessed.
   */
  def apply(s: String): Option[Document] = apply(new File(s))

  /**
   * Returns the documents corresponding to some files under a given path.
   * <p>
   * This is an alias on <code>fetchDir(File, Boolean)</code>.
   * It allows to write <code>fetch("my/path")</code> instead of
   * <code>fetchDir(new File("my/path"), true)</code>.
   *
   * @param dir the name of the directory to search under
   * @param recursive if the fetching must continue in the directories of <code>dir</code>
   * @return a [[scala.collection.Seq]] of [[textmining.Document]]
   * @throws java.io.FileNotFoundException if the <code>s</code> argument is not a valid pathname.
   */
  def fetch(dir: String, recursive: Boolean = true): Seq[Document] = {
    val file = new File(dir)
    if (file.exists()) fetchDir(new File(dir), recursive) else
      throw new FileNotFoundException(dir + " is not a valid pathname")
  }

  /**
   * Returns a sequence of documents representing readable text files
   * located immediately under <code>dir</code> (unless
   * <code>recursive</code> is set).
   *
   * @param dir the directory to search in
   * @param recursive whether the crawling must be recursive or not
   * @return a [[scala.collection.Seq]] of [[textmining.Document]]
   * @throws SecurityException if a security manager exists and its
   *                           <code>SecurityManager.checkRead(String)</code>
   *                           method denies read access to a directory under
   *                           <code>dir</code>.
   */
  def fetchDir(dir: File, recursive: Boolean): Seq[Document] = {
    val children = dir.listFiles()
    var xs = for (Some(c) <- children.map(Document(_))) yield c

    xs = if (!recursive) xs
    else
      xs ++ children.withFilter(
        (d: File) => d.canExecute && d.canRead && d.isDirectory
      ).flatMap(fetchDir(_, recursive = true))
    xs
  }
}