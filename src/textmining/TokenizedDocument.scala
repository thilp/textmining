package textmining

class TokenizedDocument(val url: String, val words: Array[String])

object TokenizedDocument {
  val wordPattern = """\p{L}+""".r
  val wordSeparator = """\P{L}+""".r

  def apply(url: String, text: String) =
    new TokenizedDocument(url, wordSeparator split text)

  def analyze(documents: Seq[Document], processes: Array[TextProcessor]): Seq[TokenizedDocument] = {
    for (doc <- documents) yield
      TokenizedDocument(doc.url,
        wordPattern.replaceAllIn(
          doc.text, (m) => (m.toString() /: processes)((s, p) => p process s)
        )
      )
  }

}