package textmining

import scala.collection.mutable

object QueryParser {

  sealed abstract class Expr
  case class Not(expr: Expr) extends Expr {
    override def toString: String = "(Not " + expr.toString + ")"
  }
  case class Or(left: Expr, right: Expr) extends Expr {
    override def toString: String = "(Or " + left.toString + " " + right.toString + ")"
  }
  case class And(left: Expr, right: Expr) extends Expr {
    override def toString: String = "(And " + left.toString + " " + right.toString + ")"
  }
  case class Term(string: String) extends Expr {
    override def toString: String = "<" + string + ">"
  }

  def wellBalanced(array: Array[String]): Boolean = {
    val stack = new mutable.ArrayStack[Boolean]
    array.foreach {
      case "(" => stack push true
      case ")" => if (stack.nonEmpty) stack.pop() else return false
      case _ => ()
    }
    stack.isEmpty
  }

  def parse(s: String): Expr = {
    val tokens = ("""\p{LD}+|[()]""".r findAllIn s).toArray[String]
    if (!wellBalanced(tokens)) throw new Exception("Unbalanced input")
    val (ast, _) = parseRec(tokens)
    ast
  }

  def parseRec(array: Array[String]): (Expr, Int) = {
    if (array.length == 1) Pair(Term(array(0)), 1)
    else array(0) match {
      case "(" => {
        array(1) match {
          case "AND" => {
            val (e1, n1) = parseRec(array.drop(2))
            val (e2, n2) = parseRec(array.drop(2 + n1))
            Pair(And(e1, e2), 3 + n1 + n2)
          }
          case "OR" => {
            val (e1, n1) = parseRec(array.drop(2))
            val (e2, n2) = parseRec(array.drop(2 + n1))
            Pair(Or(e1, e2), 3 + n1 + n2)
          }
          case "NOT" => {
            val (e, n) = parseRec(array.drop(2))
            Pair(Not(e), 2 + n)
          }
          case other => throw new Exception(s"Unknown operator $other")
        }
      }
      case e: String => Pair(Term(e), 1)
    }
  }
}