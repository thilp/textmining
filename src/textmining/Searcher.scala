package textmining

import QueryParser._

/**
 * Uses a [[textmining.Index]] to associate user queries to URLs.
 * @param index the [[textmining.Index]] used by this Searcher
 */
class Searcher(index: Index) {

  /**
   * Wrapper around search(root: Expr): Set[String] to allow comfortable search from a string instead of an AST.
   * @param query the words to search for
   * @return a set of matching URLs
   */
  def search(query: String): Set[String] = search(QueryParser.parse(query))

  /**
   * Given query (as an Expr AST), returns the set of URLs that match that query.
   * @param root Root node of the query
   * @return a set of matching URLs
   * @throws Predef.NoSuchElementException if the textmining.Index is inconsistent
   */
  def search(root: Expr): Set[String] = root match {
    case And(a, b) => search(a) & search(b)
    case Or(a, b) => search(a) | search(b)
    case Not(a) => index.urlToDid.keySet &~ search(a)
    case Term(s) => if (index.wordToDids.contains(s)) index.wordToDids(s) map (
      (a) => index.urlToDid.find {
        case (k: String, v: Int) => v == a
      }.get._1
    ) else Set()
  }
}
