package textmining

import java.text.Normalizer.Form._
import java.text.Normalizer.normalize

class Normalizer extends TextProcessor {
  override def process(word: String): String =
      normalize(word, NFD).replaceAll( """\p{M}+""", "").toLowerCase
}