import textmining._

object Main extends App {

  val DIRECTORY = "/tmp/20news-bydate-train" // Where the program is going to index the files
  val QUERIES = Array("farewell", "(AND thinking imaginative)") // Which queries to perform on them
  val SAVE = "/tmp/index.save" // Where to save the serialized index

  val eol = java.lang.System.getProperty("line.separator")

  // Using two different scopes before and after serialization ensures
  // that we don't just use always the same variable,
  // but the deserialized structure instead.
  {
    println(s"Fetching all files in $DIRECTORY…")
    val docs = Document.fetch(DIRECTORY)

    println("Tokenization…")
    val tokDocs = TokenizedDocument.analyze(
      docs, Array(new Normalizer)
    )

    println("Postings…")
    val postings = Posting.index(tokDocs)

    println("Indexing…")
    val index = Index.build(postings)

    println("Serializing index…")
    index.save(SAVE)
  } // From now the "index" value doesn't exist anymore.

  {
    println("Deserializing index…")
    val index = Index.load(SAVE)

    val searcher = new Searcher(index)

    for (query <- QUERIES) {
      println(s"""$eol* Executing a new query: "$query"…""")
      val found = searcher.search(query)

      if (found.isEmpty) println("No match!") else
        println(found.mkString(s"The following URLs match your query:$eol\t", s"$eol\t", s"$eol---"))
    }
  }

  println(eol + "Job done, goodbye!")

}
