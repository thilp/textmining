# Makefile for Scala projects distribution
# Thibaut Le Page <lepage_a@yaka.epita.fr>, June 30 2013

-include Makefile.rules

COMPILER  ?= scalac
COMPFLAGS  = -optimise
RUNTIME   ?= java
RFLAGS     =

SCALALIB ?= /usr/share/scala/lib/scala-library.jar

SRCDIR   = src
CLASSDIR = build
DOCDIR   = doc

DOCPROC   ?= scaladoc
DOCFLAGS   = -doc-title "Textmining project documentation"
DOCTARGETS = $(SRCDIR)/Main.scala $(SRCDIR)/textmining/*

TARNAME = lepage_a-Textmining
TARBALL = $(TARNAME).tar.gz

all: run

run: $(CLASSDIR)/Main.class
	$(RUNTIME) -classpath $(SCALALIB):$(CLASSDIR) Main

.PHONY: run

$(CLASSDIR)/Main.class: $(SRCDIR)/Main.scala
	test -d $(CLASSDIR) || mkdir -p $(CLASSDIR)
	$(COMPILER) $(COMPFLAGS) -sourcepath $(SRCDIR) -d $(CLASSDIR) $<

doc: $(DOCTARGETS)
	test -d $(DOCDIR) || mkdir -p $(DOCDIR)
	$(DOCPROC) -d $(DOCDIR) $(DOCFLAGS) $(DOCTARGETS)
	@echo "You can now view the documentation with '<your-browser> $(DOCDIR)/index.html'"

clean:

distclean: clean
	-rm -r $(DOCDIR)
	-rm -r $(CLASSDIR)
	-rm Makefile.rules
	-rm $(TARBALL)

.PHONY: clean distclean

dist: distclean
	tar cf $(TARNAME).tar *
	if test -d $(TARNAME); then rm -r $(TARNAME); fi
	mkdir $(TARNAME)
	tar xf $(TARNAME).tar -C $(TARNAME)
	tar cf - "$(TARNAME)" | gzip -c > $(TARBALL)
	rm -r $(TARNAME) $(TARNAME).tar

.PHONY: dist

distcheck: dist
	tar xf $(TARBALL)
	test -f $(TARNAME)/configure \
	    && test -x $(TARNAME)/configure \
	    && sh -C $(TARNAME)/configure
	make -C $(TARNAME) all
	make -C $(TARNAME) dist
	rm -rf $(TARNAME)
	@echo "--->  $(TARBALL) ready for distribution!"
